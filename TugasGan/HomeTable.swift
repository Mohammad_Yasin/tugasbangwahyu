//
//  HomeTable.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 09/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

class HomeTable: UITableView,UITableViewDataSource,UITableViewDelegate{
    
    override func awakeFromNib() {
    
        setupTableView()

    }
    
    func setupTableView() {
        self.delegate = self
        self.dataSource = self
        self.tableFooterView = UIView()
        self.separatorColor = .clear
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = (Bundle.main.loadNibNamed("HeaderHomeView", owner: self , options: nil)?[0] as! HeaderHomeView)
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 100)
        
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = indexPath.row
//        tableView.separatorColor = .clear
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AsianTableViewCell") as! AsianTableViewCell
//        cell.selectionStyle = .none
//        return cell
        
        if row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AsianTableViewCell") as! AsianTableViewCell
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 300
                
                
            }
            return cell
        }else {

          
                let cell = tableView.dequeueReusableCell(withIdentifier: "EuropeTableViewCell") as! EuropeTableViewCell
            
                  return cell
                
            

            }

        }

    
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 300
    
    
}
    
    
}

    

