//
//  ContactUsViewController.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 14/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var view1: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        view1.roundCorners(cornerRadius: 20.0)
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .clear
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsTableViewCell") as! ContactUsTableViewCell
        cell.selectionStyle = .none
        self.tableView.separatorStyle = .none
        
        let backgroundView = cell.viewWithTag(1) as! UIView
        let backgroundImage = cell.viewWithTag(2) as! UIImageView

        backgroundView.roundedView(radius: 12)
        backgroundImage.roundedImage(radius: 10)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 217
        
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
}
