//
//  AboutUsViewController.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 11/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var viewDasarNya: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var logoWave: UIImageView!
    @IBOutlet weak var fdesc: UILabel!
    @IBOutlet weak var tinggiNye: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.roundedView(radius: 12)
        logoWave.roundCorn(cornerRadius: 10)
        
        
//        let basicHeight = CGFloat(812)
//        let labelHeight = fdesc.text?.height(withConstrainedWidth: scrollView.frame.size.width, font: UIFont.systemFont(ofSize: 14)) ?? 0
//        let scrollHeight = basicHeight + labelHeight
//        tinggiNye.constant = scrollHeight
        

    }
    
    override func viewDidLayoutSubviews() {
        viewDasarNya.roundCorners(cornerRadius: 20.0)
        
    }

    @IBAction func backButton(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
    

}
