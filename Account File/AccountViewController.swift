//
//  AccountViewController.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 10/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit
import MXParallaxHeader


class AccountViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    

    @IBOutlet weak var photoProfile: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let ijinNin = MXParallaxHeader()
    
    override func viewDidLoad() {
        super.viewDidLoad()


        photoProfile.circleImage()
        setupTableView()
        
//        view1.contentMode = .scaleAspectFill
//        tableView.parallaxHeader.view = view1
//        tableView.parallaxHeader.height = 100
//        tableView.parallaxHeader.minimumHeight = 200
//        tableView.parallaxHeader.mode = .topFill

    }
    
    override func viewDidLayoutSubviews() {
        view1.roundCorners(cornerRadius: 28)
//        view2.roundCorners(cornerRadius: 20.0)
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .clear
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
            let backgroundView = cell.viewWithTag(9)
            backgroundView?.roundCorners(cornerRadius: 28)
            
        
            return cell
        } else {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 71
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.tableView.separatorStyle = .none
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccountTableViewCell") as! AccountTableViewCell
            cell.selectionStyle = .none
            
            
            return cell
            
        } else {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccountTableViewCell2") as! AccountTableViewCell2
            cell.selectionStyle = .none
            
            
            let backgroundView = cell.viewWithTag(1) as! UIView
            let backgroundImage = cell.viewWithTag(5) as! UIView
            let label = cell.viewWithTag(2) as! UILabel
            let label2 = cell.viewWithTag(3) as! UILabel
            let label3 = cell.viewWithTag(4) as! UILabel
            let label4 = cell.viewWithTag(6) as! UILabel
            
//            let character = sampleData[indexPath.row]
//            
//            label.text = character ["jam"] as? String
//            label2.text = character ["nama"] as? String
//            label3.text = character ["waktu"] as? String
//            label4.text = character ["ket"] as? String
            
            
            
            
            backgroundView.roundedView(radius: 10)
            backgroundImage.roundedView(radius: 10)
            

            self.tableView.rowHeight = 87
            return cell
            
            
        }
        return UITableViewCell()
        
      
    }
    
   
    @IBAction func setting(_ sender: Any) {
        
        let uhuh = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as? SettingViewController
        
        self.navigationController?.pushViewController(uhuh!, animated: true)
        
    }
    
    @IBAction func aboutUs(_ sender: Any) {
        
        
        let uhuh = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
        
        self.navigationController?.pushViewController(uhuh!, animated: true)
    }
    
    
    @IBAction func more(_ sender: Any) {
        
        let uhuh = self.storyboard?.instantiateViewController(withIdentifier: "MoreViewController") as? MoreViewController
        
        self.navigationController?.pushViewController(uhuh!, animated: true)
    }
    
    
    @IBAction func notif(_ sender: Any) {
        
        let uhuh = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
        
        self.navigationController?.pushViewController(uhuh!, animated: true)
    }
    
    
    @IBAction func contactUs(_ sender: Any) {
        
        let uhuh = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as? ContactUsViewController
        
        self.navigationController?.pushViewController(uhuh!, animated: true)
        
    }
   
    
//    @IBAction func push(_ sender: Any) {
//        
//        let source = NotificationViewController()
//        let notifButton = UIStoryboard(name: "Account", bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
//    }
//    
    
}
