//
//  SettingViewController.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 15/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var imagePP: UIImageView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var editProfile: UIButton!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var learnMore: UIView!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var viewSave: UIView!
    
    @IBOutlet weak var mySwitch: UISwitch!
    
    @IBOutlet weak var viewWorldPrior: UIView!
    
    @IBOutlet weak var world_prio: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePP.circleImage()
        view2.circleView()
        learnMore.roundedView(radius: 8)
        viewCancel.roundedView(radius: 9)
        viewCancel.backgroundBorder(color: #colorLiteral(red: 0.8784313725, green: 0.8784313725, blue: 0.8784313725, alpha: 1), width: 1)
        viewSave.roundedView(radius: 9)
        settingMySwitch()
        world_prio.roundedImage(radius: 10)
        
    }
    
    func settingMySwitch (){
        
        mySwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        mySwitch.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mySwitch.onTintColor = #colorLiteral(red: 0.2980392157, green: 0.2980392157, blue: 0.2980392157, alpha: 1)
        mySwitch.thumbTintColor = #colorLiteral(red: 0, green: 0.4274509804, blue: 0.4039215686, alpha: 1)
        mySwitch.backgroundBorder(color: #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1), width: 1)
        mySwitch.roundedView(radius: 16)
        mySwitch.layer.masksToBounds = true
    }
    
    override func viewDidLayoutSubviews() {
        view1.roundCorners(cornerRadius: 20.0)
        view2.backgroundBorder(color: #colorLiteral(red: 0, green: 0.4974527359, blue: 0.4798955321, alpha: 1), width: 2)
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editButton(_ sender: Any) {
    }
}
