//
//  MoreViewController.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 12/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var viewLogOut: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingViewLogOut()
        setupTableView()
        
    }
    
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .clear
    }
    
    
    func settingViewLogOut (){
        
        viewLogOut.roundedView(radius: 10)
        viewLogOut.layer.shadowColor = UIColor.lightGray.cgColor
        viewLogOut.layer.shadowOpacity = 0.5
        viewLogOut.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    override func viewDidLayoutSubviews() {
        view1.roundCorners(cornerRadius: 20.0)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        self.tableView.separatorStyle = .none
        
        if row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacyPolicyTableViewCell") as! PrivacyPolicyTableViewCell
            cell.selectionStyle = .none
            
            let button = cell.viewWithTag(1) as! UIButton
         
            return cell
            
        }else if row == 1 {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TermsConditionsTableViewCell") as! TermsConditionsTableViewCell
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if row == 2 {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TermsOfUsersTableViewCell") as! TermsOfUsersTableViewCell
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if row == 3 {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTableViewCell") as! HelpTableViewCell
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if row == 4 {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeLanguangeTableViewCell") as! ChangeLanguangeTableViewCell
            cell.selectionStyle = .none
            
            return cell
            
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    @IBAction func backButton(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
    
    

}
