//
//  UIView.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 09/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

extension UIView {

    func roundedView(radius: CGFloat) {
        self.layer.cornerRadius = radius
        
        
    }
    
    func roundedTop() {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight , .topLeft], cornerRadii: CGSize(width: self.frame.width/20.0, height: self.frame.height/20.0)).cgPath
        //Here I'm masking the textView's layer with rectShape layer
        self.layer.mask = rectShape
    }
    
    func roundCorners(cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func circleView(){
        
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
        
    }
    
    func backgroundBorder(color: CGColor, width: CGFloat) {
        self.layer.borderColor = color
        self.layer.borderWidth = width
    }
}
