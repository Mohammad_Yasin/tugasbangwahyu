//
//  UIImageView.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 09/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func roundedImage(radius: CGFloat) {
        
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        
    }
    
    func circleImage(){
        
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
        
    }
    
    func roundCorn(cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
}
