//
//  AsiansCollection.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 08/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

class AsiansCollection: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var sampleData = [
        
        [
            
            "picture"   :  #imageLiteral(resourceName: "singapo") ,
            "nama"      : "Condet Macan Tutul",
            "logo"   :  #imageLiteral(resourceName: "singapor") ,
            "keterangan"    : "Condet City",
            "harga"    :  "IDR 251.000.000",
            
            
            ],
        
        [
            
            "picture"   :  #imageLiteral(resourceName: "mars") ,
            "nama"      : "Mars Condet",
            "logo"   :  #imageLiteral(resourceName: "usa") ,
            "keterangan"    : "Shopping outlet",
            "harga"    :  "IDR 53.690.000",
            
            
            ]
        
        ]

    
    override func awakeFromNib() {
        
        self.delegate = self
        self.dataSource = self
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sampleData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AsianCollectionViewCell", for: indexPath) as! AsianCollectionViewCell
        
        let image = cell.viewWithTag(1) as! UIImageView
        let backgroundView = cell.viewWithTag(2) as! UIView
        let backgroundView2 = cell.viewWithTag(3) as! UIView
        let nama = cell.viewWithTag(4) as! UILabel
        let keterangan = cell.viewWithTag(5) as! UILabel
        let harga = cell.viewWithTag(6) as! UILabel
        let logo = cell.viewWithTag(7) as! UIImageView
        
        let model = sampleData[indexPath.row]
        
        image.image = model ["picture"] as? UIImage
        nama.text = model ["nama"] as? String
        keterangan.text = model ["keterangan"] as? String
        harga.text = model ["harga"] as? String
        logo.image = model ["logo"] as? UIImage
        
        
        backgroundView.roundedView(radius: 10)
        backgroundView2.roundedView(radius: 10)
        image.roundedImage(radius: 20)
        
        return cell
    }
    

   
}
