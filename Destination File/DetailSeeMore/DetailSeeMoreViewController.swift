//
//  DetailSeeMoreViewController.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 09/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

class DetailSeeMoreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()

    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .clear
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailSeeMoreTableViewCell") as! DetailSeeMoreTableViewCell
        cell.selectionStyle = .none
        self.tableView.separatorStyle = .none
    
        
        
        return cell
       
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
        
        
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
}




