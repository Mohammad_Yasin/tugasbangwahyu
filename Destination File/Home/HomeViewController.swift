//
//  HomeViewController.swift
//  TugasGan
//
//  Created by Mohammad Yasin on 06/09/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        let nib = UINib(nibName: "HeaderHomeView", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "HeaderHomeView")
       
       
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .clear
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = (Bundle.main.loadNibNamed("HeaderHomeView", owner: self , options: nil)?[0] as! HeaderHomeView)
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 100)
        
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = indexPath.row
        
        self.tableView.separatorStyle = .none
    
        if row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AsianTableViewCell") as! AsianTableViewCell
            cell.selectionStyle = .none
            
            return cell
            
        }else if row == 1 {
            
  
            let cell = tableView.dequeueReusableCell(withIdentifier: "EuropeTableViewCell") as! EuropeTableViewCell
            cell.selectionStyle = .none
            
            return cell
            

        }else if row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AustraliaTableViewCell") as! AustraliaTableViewCell
             cell.selectionStyle = .none
            
            return cell
            
        }
        
        return UITableViewCell()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
        
        
    }
    
    @IBAction func pushButton(_ sender: Any) {
        
        let uhuh = self.storyboard?.instantiateViewController(withIdentifier: "DetailSeeMoreViewController") as? DetailSeeMoreViewController
        
        self.navigationController?.pushViewController(uhuh!, animated: true)
    }
    
    @IBAction func pushPushan(_ sender: Any) {
        
        let uhuh = self.storyboard?.instantiateViewController(withIdentifier: "DetailSeeMoreViewController") as? DetailSeeMoreViewController
        
        self.navigationController?.pushViewController(uhuh!, animated: true)
    }
    @IBAction func pushApin(_ sender: Any) {
        
        let uhuh = self.storyboard?.instantiateViewController(withIdentifier: "DetailSeeMoreViewController") as? DetailSeeMoreViewController
        
        self.navigationController?.pushViewController(uhuh!, animated: true)
    }
}



